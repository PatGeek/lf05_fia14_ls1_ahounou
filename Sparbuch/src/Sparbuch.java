
public class Sparbuch {
	private int kontonummer;
	private double kapital,zinsatz,jahr;

	public Sparbuch() {
		// TODO Auto-generated constructor stub
	}
	public Sparbuch(int ktnr, double kap, double zins) {
		setZinsatz(zins);
		setKapital(kap);
		// TODO Auto-generated constructor stub
	}
	public int getKontonummer() {
		return kontonummer;
	}
	public void setKontonummer(int kontonummer) {
		this.kontonummer = kontonummer;
	}
	public double getZinsatz() {
		return zinsatz;
	}
	public void setZinsatz(double zinsatz) {
		this.zinsatz = zinsatz;
	}
	public double getJahr() {
		return jahr;
	}
	public void setJahr(double jahr) {
		this.jahr = jahr;
	}
	public double getKapital() {
		return kapital;
	}
	public void setKapital(double Betrag) {
		this.kapital += Betrag;
	}
	
	public void zahleEin( double Betrag) {
		setKapital( Betrag);
	}
	public void hebeAb( double Betrag) {
		setKapital(- Betrag);
	}
	
	 public double getErtrag(double Jahr) {
		this.jahr=Jahr;
		return getKapital()* Math.pow(zinsatz/100, Jahr);
	}
	 
	 public void verzinse() {
		setKapital(getErtrag(jahr));
	}


}
