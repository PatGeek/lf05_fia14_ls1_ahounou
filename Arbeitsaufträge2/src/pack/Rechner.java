package pack;

import java.util.Scanner;

public class Rechner {

	public static void main(String[] args) // Hier startet das Programm
	{
		//Uebung1();
		Uebung2();

	}

	private static void Uebung2() {
		
		String name,Alter;
		Ausgabe("Hallo.\nWie hei�t du?");
		name=myscanner();
		Ausgabe("\nHi"+ name+". Wie alt bist du?");
		Alter=myscanner();
		
		System.out.printf("Er hei�t %s und ist %s Jahre alt",name,Alter);

	}

	private static String myscanner() {
		String Scan;
		Scanner myScanner = new Scanner(System.in);
		Scan=  myScanner.next();
		return Scan;
	}

	private static void Ausgabe(String Text) {

	System.out.print(Text);
	}

	private static void Uebung1() {

		// Neues Scanner-Objekt myScanner wird erstellt
		Scanner myScanner = new Scanner(System.in);

		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

		// Die Variable zahl1 speichert die erste Eingabe
		int zahl1 = myScanner.nextInt();

		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

		// Die Variable zahl2 speichert die zweite Eingabe
		int zahl2 = myScanner.nextInt();

		// Die Addition der Variablen zahl1 und zahl2
		// wird der Variable ergebnis zugewiesen.
		int ergebnis = zahl1 + zahl2;

		System.out.print("\n\n\nErgebnis der Addition lautet: ");
		System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);
		myScanner.close();

	}
}
