﻿import java.util.Iterator;
import java.util.Scanner;
import java.util.ArrayList;

class Fahrkartenautomat
{
	static ArrayList<Double> Zwischensumme = new ArrayList<Double>();
    static double BEstellungGesamt;
    static int Tickets;
      
     
    static double zuZahlenderBetrag; 
    static double eingezahlterGesamtbetrag;
    static double eingeworfeneMünze;
    static double rückgabebetrag;    
     static Scanner tastatur = new Scanner(System.in);

    public static void main(String[] args)
    {
    	 //Vorgang(); 
    	Fahrkartenart ();    	
    	GeldEinwurf();
    	Rückgeldberechnung();
    	
    }
    
    public static void  Rückgeldberechnung() {
		
    	// Rückgeldberechnung und -Ausgabe
        // -------------------------------
    	double zuZahlenderBetrag;
    	zuZahlenderBetrag=BEstellungGesamt;  
        rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        
        if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO \n",rückgabebetrag );
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }
        
        Fahrscheinausgabe();
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
	}
    
  
    
    public static void GeldEinwurf() {
    	 System.out.printf("Zu zahlender Betrag %.2f EURO: \n",BEstellungGesamt);
         zuZahlenderBetrag = BEstellungGesamt;

         // Geldeinwurf
         // -----------
         eingezahlterGesamtbetrag = 0.0;
         while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
         {
      	   System.out.printf("\nNoch zu zahlen: %.2f EURO \n" ,(zuZahlenderBetrag - eingezahlterGesamtbetrag));
      	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
      	   eingeworfeneMünze = tastatur.nextDouble();
             eingezahlterGesamtbetrag += eingeworfeneMünze;
         }
	}

	//Anzahl Tickets
    public static int AnzahlTicket() {
    	System.out.print("Anzahl der Tickets: ");
    	int AnzahlT =tastatur.nextInt();
    	try {
    		
        	if (AnzahlT<0||AnzahlT>10) {
        		    		
        			System.out.println("Sie haben einen ungültigen Wert eingegeben."
            				+ "Die Anzahl der Tickets ist in diesem Fall mit dem Standardwert 1");
            		AnzahlT=1;
        	}    	    		
			
		} catch (Exception e) {
			// TODO: handle exception
		}
          	
    	 return AnzahlT;
	}
    
    public static void Uploading(String Text) {    	
    	 System.out.println(Text);
         for (int i = 0; i <Text.length(); i++)
         {
            System.out.print("=");
                        	
            try {
  			Thread.sleep(250);
  		} catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
       }
         System.out.println("\n");	
	}
    
    public static void Vorgang() {
    	 String Text= "Fahrkartenbestellvorgang:"; 
         Uploading(Text);
	}
    public static void bezahlen() {
    	String Text="\nJetzt geht's weiter mit der Zahlung";
    	if (BEstellungGesamt==0) {
    		System.out.println("Sie müssen eine Fahrkarte auswählen");
    		Fahrkartenart();
		} else {
			System.out.printf("\nGesamtsumme:  %.2f EURO\n",BEstellungGesamt );			
			Uploading(Text);
		}
		
	}
    
    public static void Summe(double Preis, int AnzahlT) {
    	double Summe;
    	
    	Summe= Preis*AnzahlT  ;    	
    	System.out.printf("Summe: %.2f EURO\n\n",Summe );    	
    	Zwischensumme.add(Summe); 
    	fahrkartenbestellungErfassen ();
		
	
	}
	
    public static double fahrkartenbestellungErfassen () {  	
    	
    	Iterator<Double> it = Zwischensumme.iterator();
    	while(it.hasNext()) {
    		BEstellungGesamt+= (Double)it.next();
    		}
    	System.out.printf("Zwischensumme: %.2f EURO\n\n",BEstellungGesamt );
    	
    	Fahrkartenart();
		return BEstellungGesamt;
	}
    
    
    public static void Fahrkartenart () {
    	   int Wahl;
    	System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
    	String arr[] = new String[11];
    	arr[1]= "Einzelfahrschein Regeltarif AB [2,90 EUR] (1)";
    	arr[2]= "Tageskarte Regeltarif AB [8,60 EUR] (2)";
    	arr[3]= "Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)";
    	arr[4]= "Kurzstrecke [1,90] (4)";
    	arr[5]= "Tageskarte Berlin AB [8,60] (5)";
    	arr[6]= "Tageskarte Berlin BC [9,00] (6)";
        arr[7]= "Tageskarte Berlin ABC [9,60] (7)";
        arr[8]= "Kleingruppen-Tageskarte Berlin AB [23,50] (8)";
        arr[9]= "Kleingruppen-Tageskarte Berlin BC [24,30] (9)";
        arr[10]= "Kleingruppen-Tageskarte Berlin ABC	[24,90] (10)";
    	arr[0]= "Bezahlen (9)";
    	for (int i = 0; i < arr.length; i++) {
    		System.out.println(" "+ arr[i]);
		}
    	System.out.print("\nIhre Wahl: ");
    	Wahl=tastatur.nextInt();
    	
    	
    	switch (Wahl) {
		case 1:
			Summe(2.9,AnzahlTicket());			
			break;
		case 2:
			Summe(8.6,AnzahlTicket());			 
			break;
		case 3:
			Summe(23.5,AnzahlTicket());
			break;
		case 9:
			bezahlen();
			break;
		
		default:
			System.out.print(">>falsche Eingabe<<\n\n");	
			Fahrkartenart ();
			break;
			
		}
    	
	}
    
   
    public static void Fahrscheinausgabe() {    	
    	 // Fahrscheinausgabe
        // -----------------
    	String Text= "\nFahrschein wird ausgegeben"; 
    	Uploading(Text);
	}
}

/* Begründen Sie Ihre Entscheidung für die Wahl des Datentyps
/*Ein Ticket ist eine Ganzzahl
 Erläutern Sie detailliert, was bei der Berechnung des Ausdrucks anzahl * einzelpreis passiert.
 Der zuzahlender Betrag soll berechnet werden*/


