
public class Konfiguration {

	
	public static void main(String[] args) {
		String name;
		char sprachModul = 'd';
		boolean status;
		double prozent, summe, euro, cent;
		String typ = "Automat AVR";
		String bezeichnung = "Q2021_FAB_A";
		name = typ + " " + bezeichnung;
		int muenzenCent = 1280;
		int muenzenEuro = 130;
		double maximum = 100.00;
		double patrone = 46.24;

		summe = muenzenCent + muenzenEuro * 100;
		cent = summe % 100;
		prozent = maximum - patrone;
		euro = summe / 100;

		final byte PRUEFNR = 4;

		status = (euro <= 150) && (prozent >= 50.00) && (euro >= 50) && (cent != 0) && (sprachModul == 'd')
				&& (!(PRUEFNR == 5 || PRUEFNR == 6));

		System.out.println("Name: " + name);
		System.out.println("Sprache: " + sprachModul);
		System.out.println("Pruefnummer : " + PRUEFNR);
		System.out.println("Fuellstand Patrone: " + prozent + " %");
		System.out.println("Summe Euro: " + euro + " Euro");
		System.out.println("Summe Rest: " + cent + " Cent");
		System.out.println("Status: " + status);
	}

}
