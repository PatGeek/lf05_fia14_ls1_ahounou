package Paket;

public class Konsolenausgabe1 {

	public static void main(String[] args) {
		A1_2();
		Ub�ng1();
		
		System.out.printf ("Ach du gr�ne %6.2f", 9.8765);

	}

	// String als Parameter
	public static void Ausgabe(String Text) {
		System.out.print(Text);
	}

	public static void AusgabeNewLine(String Text) {
		System.out.println(Text);
	}

	public static void A1_2() {
		Ausgabe("Guten Tag Patrick \n");
		Ausgabe("Willkommen in der Veranstaltung Strukturierte Programmierung. \n \n");
	}

	public static void Ub�ng1() {
		U_Aufgabe1();
		U_Aufgabe2();
		U_Aufgabe3();
	}

	public static void U_Aufgabe1() {
		String text1 = "Du bist ein Sieger. ";
		String Text2 = "Gib nie auf!!!";
		Ausgabe(text1 + Text2 + "\n");

		// println() f�r eine neue Zeile
		System.out.println(text1);
		System.out.println(Text2 + "\n");
	}

	public static void U_Aufgabe2() {

		int Gstern = 9;// muss ungerade und h�her als 5 sein
		for (int aktuelZeil = 0; aktuelZeil < Gstern; aktuelZeil++) {
			Zeile(Gstern, aktuelZeil);
		}
		fu�(Gstern);
		fu�(Gstern);
		AusgabeNewLine("");

	}

	static void fu�(int Gstern) {
		int SternEspace = Gstern / 3;
		for (int i = 0; i < SternEspace; i++) {
			Ausgabe(" ");
		}
		for (int i = 0; i < Gstern - (SternEspace * 2); i++) {
			Ausgabe("*");
		}
		AusgabeNewLine("");
	}

	static void Zeile(int Line, int aktuel) {
		int stern = (aktuel * 2) - 1;
		if (aktuel == 1) {
			AusgabeNewLine("");
			for (int i = 0; i < (Line - aktuel) / 2; i++) {
				Ausgabe(" ");
			}
			Ausgabe("*");
			AusgabeNewLine(" ");
		} else {

			if (stern <= Line) {
				for (int i = 0; i < (Line - stern) / 2; i++) {
					Ausgabe(" ");
				}
				for (int i = 0; i < stern; i++) {
					Ausgabe("*");
				}
				AusgabeNewLine("");
			}

		}

	}
     
	private static void U_Aufgabe3() {
		Format(22.4234234);

	}
	private static void Format(double Zahl) {
		System.out.format("%2f", Zahl);
	}
}
