package Package;

import java.util.Scanner;

public class AB32 {
	
	static Scanner myScanner = new Scanner(System.in);
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Aufgabe1();
		//Aufgabe2();
		//Aufgabe3();
		//Aufgabe4();
		Aufgabe5();

	}
	
	static double berechneMittelwert(double y,double x) {
		
		return (x+y)/2.0;
	}
	
	 static void Aufgabe1() {
		 double m= berechneMittelwert(2, 2);
		 System.out.println("berechneMittelwert: "+m);
	 }
	

		
	 
	 public static void Aufgabe2() {	 	  
		 
			// Benutzereingaben lesen
			String artikel=liesString("was m�chten Sie bestellen?");
			int anzahl= liesInt("Geben Sie die Anzahl ein:");
			double nettogesamtpreis=liesDouble("Geben Sie den Nettopreis ein:");
			double mwst=liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");			

			// Verarbeiten
			double GesamtPreis= berechneGesamtnettopreis(anzahl, nettogesamtpreis);
			double bruttogesamtpreis= berechneGesamtbruttopreis( GesamtPreis, mwst);		

			// Ausgeben
			System.out.println("\tRechnung");
			System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
			System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");	
			
	}
	 
	 public static void Aufgabe3() {
		 double R1,R2;
		 R1= liesDouble("R1 eingeben bitte");
		 R2= liesDouble("R2 eingeben bitte");
	     reihenschaltung(R1,R2);
	 }
	 public static void Aufgabe4() {
		 double R1,R2;
		 R1= liesDouble("R1 eingeben bitte");
		 R2= liesDouble("R2 eingeben bitte");
	     parallelsschaltung(R1,R2);
	 }
	 
	 public static void Aufgabe5() {
		 double X;
		 X= liesDouble("Quadrat von");
		 quadrat(X);
		 System.out.println("Quadrat: " + quadrat(X));
	 }
	 
	 
	 
		 
	 
	 
	 public static String liesString(String text) {
			System.out.println(text);
			String artikel = myScanner.next();
			return artikel;
		}
	 
	 public static int liesInt(String text) {
		 System.out.println(text);
		int anzahl = myScanner.nextInt();
		return anzahl;
	 }
	 public static double liesDouble(String text) {
		 System.out.println(text);
			double preis = myScanner.nextDouble();
			return preis;
	 }
	 public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		 double nettogesamtpreis = anzahl * nettopreis;
		 return  nettogesamtpreis;
	 }
	 public static double berechneGesamtbruttopreis(double nettogesamtpreis,  double mwst) {
		 double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		 return bruttogesamtpreis;
	 }

	 public static void reihenschaltung (double r1,double r2) {
		
		 System.out.println("Ersatswiederstand (Serie): "+(r1+r2));		 
	}
	 
	 public static void parallelsschaltung (double r1,double r2) {
		 
		 System.out.println("Ersatswiederstand (Parallel): "+(r1+r2)/(r1+r2));		 
	}
	 
	 public static double quadrat(double x) {
		
		return Math.pow(x, x);
	}
	 public static double hypotenuse(double kathete1, double kathete2) {
		 
		return kathete2;		
	}

}
