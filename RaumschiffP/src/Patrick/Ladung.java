/**
 * 
 */
package Patrick;

/**
 * @author pahounou9754
 *
 */
public class Ladung {
    private int menge;
    private String bezeichnung;

    Ladung(String bezeichnung, int menge) {
        this.bezeichnung = bezeichnung;
        this.menge = menge;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public String getBezeichnung() {
        return this.bezeichnung;
        
    }

    public void setMenge(int menge) {
        this.menge = menge;
    }

    public int getMenge() {
        return this.menge;
    }
}
