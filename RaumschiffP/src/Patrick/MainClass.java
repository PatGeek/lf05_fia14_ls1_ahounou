package Patrick;

public class MainClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		 Raumschiff r1 = new Raumschiff(100, 300, 15, 100, 100, 4,"Test1");
	        Raumschiff r2 = new Raumschiff(256, 100, 15, 100, 100, 4,"Test2");
	        r1.photonentorpedoSchiessen(r2);
	        Ladung ladung= new Ladung("Klingball", 2000);
	        r1.addLog("Testlog");
	        r1.addLadung(ladung);
	        r1.ladungsverzeichnusAusgeben();
	        r1.getLogs();
	        r1.zustandRaumschiff();
	        
	        System.out.println();
	        
	        r2.addLog("Testlog");
	        r2.addLadung(ladung);
	        r2.ladungsverzeichnusAusgeben();
	        r2.getLogs();
	        r2.zustandRaumschiff();

	}

}
