/**
 * 
 */
package Patrick;

import java.util.ArrayList;

/**
 * @author pahounou9754
 *
 */
public class Raumschiff {

	  private int energieversorgungInProzent, schildeInProzent, photonentorpedoAnzahl,
      huelleInProzent, lebenserhaltungssystemeInProzent, androidenAnzahl;
private String schiffsname;
private ArrayList<String>broadcastKommunikator = new ArrayList<String>();
private ArrayList<Ladung>ladungsverzeichnis = new ArrayList<Ladung>();
private String logVerzeichnis[] = {"Test1", "Test2", "Test3", "Test4"};

public Raumschiff(int photonentorpedoAnzahl,
                int energieversorgungInProzent,
                int schildeInProzent,
                int huelleInProzent,
                int lebenserhaltungssystemeInProzent,
                int androidenAnzahl,
                String schiffsname) {
  this.photonentorpedoAnzahl = photonentorpedoAnzahl;
  this.energieversorgungInProzent = energieversorgungInProzent;
  this.schildeInProzent = schildeInProzent;
  this.huelleInProzent = huelleInProzent;
  this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
  this.androidenAnzahl = androidenAnzahl;
  this.schiffsname = schiffsname;

}

public int getPhotonentorpedoAnzahl() {
  return this.photonentorpedoAnzahl;
}

public int getEnergieversorgungInProzent() {
  return this.energieversorgungInProzent;
}

public int getSchildeInProzent() {
  return this.schildeInProzent;
}

public int getHuelleInProzent() {
  return this.huelleInProzent;
}

public int getLebenserhaltungssystemeInProzent() {
  return this.lebenserhaltungssystemeInProzent;
}

public String getSchiffsname() {
  return this.schiffsname;
}

public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu) {
  this.photonentorpedoAnzahl = photonentorpedoAnzahlNeu;
}

public void setEnergieversorgungInProzent(int energieversorgungInProzentNeu) {
  this.energieversorgungInProzent = energieversorgungInProzentNeu;
}

public void setSchildeInProzent(int schildeInProzentNeu) {
  this.schildeInProzent = schildeInProzentNeu;
}

public void setHuelleInProzent(int huelleInProzentNeu) {
  this.huelleInProzent = huelleInProzentNeu;
}

public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzentNeu) {
  this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzentNeu;
}

public void setSchiffsname(String schiffsnameNeu) {
  this.schiffsname = schiffsnameNeu;
}

public void photonentorpedoSchiessen(Raumschiff rs) {
  rs.treffer(rs, 20);
}

public void phaserkanoneSchiessen(Raumschiff rs) {
  rs.treffer(rs, 5);
}

private void treffer(Raumschiff rs, int schaden) {
  System.out.println("Huelle vor treffer: " + getHuelleInProzent() + " Schilde vor treffer: " + getSchildeInProzent());

  int sd = rs.getSchildeInProzent() - schaden;
  if(sd > 0) {
      rs.setSchildeInProzent(sd);
  } else {
      rs.setSchildeInProzent(0);
      rs.setHuelleInProzent(rs.getHuelleInProzent() + sd);
  }

  System.out.println("Huelle nach treffer: " + getHuelleInProzent() + " Schilde nach treffer: " + getSchildeInProzent());
}

public void nachrichtAnAlle(String text) {
  System.out.println("AN ALLE! " + text);
}

public void addLadung(Ladung ladung) {
  this.ladungsverzeichnis.add(ladung);
}

public void addLog(String log) {
  this.broadcastKommunikator.add(log);
}

public void getLogs() {
  System.out.println(this.logVerzeichnis);
}

public void ladungsverzeichnusAusgeben() {
  System.out.println(this.ladungsverzeichnis.get(0));
}


public void photonentorpedosLaden(int anzahl) {
  this.photonentorpedoAnzahl += anzahl;
}

public void reparaturDurchfuehren(Boolean schutzschilde,
                                Boolean energieversorgung,
                                Boolean schiffshuelle,
                                int anzahlAndroiden) {


}

public void zustandRaumschiff() {
  System.out.printf("Schiff: %s\tSchutzschild: %d\t Energieversorgung: %d\tSchiffshülle: %d\n", this.schiffsname, this.schildeInProzent, this.energieversorgungInProzent, this.huelleInProzent);
}
}
